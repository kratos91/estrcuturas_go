package estructuras

import "fmt"

type Lista struct {
	Size      int
	FirstItem *Node
	LastItem  *Node
}

func GetNode(value int) *Node {
	var node = Node{}
	node.Value = value
	node.Next = nil

	return &node
}

func GetNewLista() *Lista {
	var lista = Lista{}
	lista.Size = 0
	lista.FirstItem = nil
	lista.LastItem = nil

	return &lista
}

//Implementacion de las operaciones CRUD
func Add(lista *Lista, node *Node) {
	if lista.FirstItem == nil {
		lista.FirstItem = node
	} else {
		lista.LastItem.Next = node
	}

	lista.LastItem = node
	lista.Size++
}

func Read(lista *Lista, index int) *Node {
	if lista.Size != 0 {
		var listExplorer = lista.FirstItem
		i := 0
		for i < index {
			listExplorer = listExplorer.Next
			i++
		}
		return listExplorer
	}
	return nil
}

func ReadAllList(lista *Lista) {
	if lista.Size != 0 {
		var listExplorer = lista.FirstItem
		i := 0
		for i < lista.Size {
			fmt.Print(listExplorer.Value, "->")
			listExplorer = listExplorer.Next
			i++
		}
	}
}

func Update(lista *Lista, index int, newValue int) {
	if lista.Size != 0 {
		var listExplorer = lista.FirstItem
		i := 0
		for i < index {
			listExplorer = listExplorer.Next
			i++
		}
		listExplorer.Value = newValue
	}
}

func Remove(lista *Lista, index int) {
	if lista.Size != 0 && index < lista.Size-1 {
		var listExplorer = lista.FirstItem
		if index == 0 {
			lista.FirstItem = lista.FirstItem.Next
			lista.Size--
		} else {
			i := 0
			for i < index-1 {
				listExplorer = listExplorer.Next
				i++
			}
			listExplorer.Next = listExplorer.Next.Next
			lista.Size--
		}
	}
}
