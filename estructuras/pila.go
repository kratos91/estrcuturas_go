package estructuras

type Node struct {
	Value int
	Next  *Node
}

type Stack struct {
	Size int
	Top  *Node
}

func GetNewStack() *Stack {
	var s = Stack{}
	s.Size = 0
	s.Top = nil

	return &s
}

func Push(s *Stack, value int) {
	var node = new(Node)
	node.Value = value
	node.Next = s.Top
	s.Top = node
	s.Size++
}

func Pop(s *Stack) int {
	var returnValue int
	if s.Size > 0 {
		var aux = new(Node)
		aux = s.Top
		returnValue = aux.Value
		s.Top = aux.Next
		s.Size--
	}
	return returnValue
}
