package estructuras

type Queue struct {
	Size  int
	Front *Node
	Tail  *Node
}

func GetNewQueue() *Queue {
	var q = Queue{}
	q.Size = 0
	q.Front = nil
	q.Tail = nil

	return &q
}

func Enqueue(q *Queue, node *Node) {
	if q.Front == nil {
		q.Front = node
	} else {
		q.Tail.Next = node
	}
	q.Tail = node
	q.Size++
}

func Dequeue(q *Queue) int {
	var returnValue int
	if q.Size > 0 {
		aux := q.Front
		returnValue = aux.Value
		q.Front = q.Front.Next
		q.Size--
	}
	return returnValue
}
