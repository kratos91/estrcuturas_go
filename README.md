# Repositorio con ejemplos de algoritmos de busqueda,ordenamiento y estructuras de datos con GOLANG

### Algoritmos de busqueda
- Busqueda binaria
### Algoritmos de ordenamiento
- Ordenamiento burbuja
- Ordenamiento por selección 
- Ordenamiento por inserción
- Ordenamiento quick sort
- Ordenamiento merge sort
- Ordenamiento multiple

### Estrucutras de datos
- Lista simple ligada
- Pilas
- Colas