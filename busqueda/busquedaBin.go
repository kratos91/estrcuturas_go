package algoritmos

func BinarySearch(arreglo []int, arraySize int, x int) int {
	leftBound, rightBound := 0, arraySize-1
	for leftBound < rightBound {
		middle := (rightBound + leftBound) / 2
		if arreglo[middle] == x {
			return middle
		} else if arreglo[middle] < x {
			leftBound = middle + 1
		} else {
			rightBound = middle - 1
		}
	}
	return -1
}
