package ordenamiento

func Multiple(arreglo []int) []int {
	posFinal:=len(arreglo)-1
	for i:=0;i<len(arreglo)/2;i++ {
		posMin:=i
		posMax:=i
		for j:=i;j<=posFinal;j++ {
			if arreglo[j]>arreglo[posMax] {
				posMax=j
			}
			if arreglo[j]<arreglo[posMin] {
				posMin=j
			}
		}
		aux:=arreglo[posMin]
		arreglo[posMin]=arreglo[i]
		arreglo[i]=aux
		if posMax==i {
			posMax=posMin
		}
		aux=arreglo[posFinal]
		arreglo[posFinal]=arreglo[posMax]
		arreglo[posMax]=aux
		posFinal--
	}
	return arreglo
}
