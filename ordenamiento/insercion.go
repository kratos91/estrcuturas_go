package ordenamiento

func Insercion(arreglo []int) []int {
	for i:=1;i<len(arreglo);i++ {
		aux:=arreglo[i]
		var j int
		for j=i-1;j>=0 && arreglo[j]>aux;j-- {
			arreglo[j+1]=arreglo[j]
		}
		arreglo[j+1]=aux
	}
	return arreglo
}
