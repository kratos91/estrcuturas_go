package ordenamiento

func intercambio(x *int, y *int) {
	aux := x
	x = y
	y = aux
}

func QuickSort(arreglo []int, primero int, ultimo int) {
	central := (primero + ultimo) / 2
	pivote := arreglo[central]

	i := primero
	j := ultimo

	//do-while estilo golang
	for ok := true; ok; ok = i <= j {
		//while estilo golang
		for arreglo[i] < pivote {
			i++
		}
		//while estilo golang
		for arreglo[j] > pivote {
			j--
		}

		if i <= j {
			intercambio(&arreglo[i], &arreglo[j])
			i++
			j--
		}
	}

	if primero < j {
		//ordena la sublista izquierda
		QuickSort(arreglo, primero, j)
	}

	if i < ultimo {
		//ordena la sublista derecha
		QuickSort(arreglo, i, ultimo)
	}
}
