package ordenamiento

func BubbleSort(arreglo []int) []int {
	for i := 0; i < len(arreglo)-1; i++ {
		for j := 0; j < len(arreglo)-i-1; j++ {
			if arreglo[j] > arreglo[j+1] {
				temp := arreglo[j]
				arreglo[j] = arreglo[j+1]
				arreglo[j+1] = temp
			}
		}
	}

	return arreglo
}
