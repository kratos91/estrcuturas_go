package ordenamiento

func Seleccion(arreglo []int)  []int {
	size:=len(arreglo)
	for i:=0;i<size;i++ {
		posMin:=i
		for j:=i+1;j<size;j++ {
			if arreglo[posMin]>arreglo[j] {
				posMin=j
			}
		}
		aux:=arreglo[i]
		arreglo[i]=arreglo[posMin]
		arreglo[posMin]=aux
	}

	return arreglo
}
