package main

import (
	"estrcuturas_go/ordenamiento"
	"fmt"
)

func verifyPerfect(number int)  {
	value:=0
	for i := 1; i < number; i++ {
		if number % i == 0 {
			value+=i
		}
	}
	if value==number {
		fmt.Println("El numero es perfecto")
	}else{
		fmt.Println("El numero no es perfecto")
	}
}

func main() {
	v:=[]int{2,7,1,4,3,5,0,8,2,-1}
	verifyPerfect(30)
	fmt.Println("Desordenado->",v)
	ordenamiento.Multiple(v)
	fmt.Println("ordenado->",v)

}
